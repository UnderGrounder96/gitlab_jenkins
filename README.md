# gitlab_jenkins

This project aims to integrate Gitlab with Jenkins CI, for development purposes.

## Requirements

- [Vagrant](https://vagrantup.com/intro)
- [VirtualBox](https://virtualbox.org)
- `vagrant-hostmanager vagrant-scp` plugins (optional)
- **More than 2 CPU cores**
- **More than 4GB of RAM**
- **More than 10 GB Disk space**

## Deploy

To run the VMs, simply execute the command:

```bash
bash vagrant_up.sh
```

## Tools

Gitlab - [http://localhost:8433](http://localhost:8433)

Jenkins - [http://localhost:8080](http://localhost:8080)
