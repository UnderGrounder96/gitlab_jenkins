# -*- mode: ruby -*-
# vi: set ft=ruby :

### configuration parameters ###
NODE_A_IP = "192.168.33.10"
NODE_A_HOST = "gitlab"

NODE_B_IP = "192.168.33.11"
NODE_B_HOST = "jenkins"

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "generic/rocky8"

  config.vm.define "gitlab", primary: true do |gitlab|
    gitlab.vm.hostname = NODE_A_HOST

    # Create a private network, which allows host-only access to the machine
    # using a specific IP.
    gitlab.vm.network "private_network", ip: NODE_A_IP

    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine and only allow access
    # via 127.0.0.1 to disable public access

    gitlab.vm.network "forwarded_port", guest: 80, host: 8000, host_ip: "127.0.0.1", auto_correct: true
    gitlab.vm.network "forwarded_port", guest: 443, host: 8443, host_ip: "127.0.0.1", auto_correct: true
    gitlab.vm.network "forwarded_port", guest: 5000, host: 5000, host_ip: "127.0.0.1", auto_correct: true
    gitlab.vm.network "forwarded_port", guest: 5050, host: 5050, host_ip: "127.0.0.1", auto_correct: true

    # Provider-specific configuration so you can fine-tune various
    # backing providers for Vagrant. These expose provider-specific options.
    # Example for VirtualBox:
    #
    gitlab.vm.provider "virtualbox" do |vb|
      vb.name = "gitlab_vm"

      # Display the VirtualBox GUI when booting the machine
      # vb.gui = true

      # Customize the amount of memory on the VM:
      vb.memory = "16384"
      vb.cpus = 6
    end
    #
    # View the documentation for the provider you are using for more
    # information on available options.
  end

  config.vm.define "jenkins" do |jenkins|
    jenkins.vm.hostname = NODE_B_HOST

    # Create a private network, which allows host-only access to the machine
    # using a specific IP.
    jenkins.vm.network "private_network", ip: NODE_B_IP

    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine and only allow access
    # via 127.0.0.1 to disable public access
    jenkins.vm.network "forwarded_port", guest: 8080, host: 8080, host_ip: "127.0.0.1", auto_correct: true
    jenkins.vm.network "forwarded_port", guest: 50000, host: 50000, host_ip: "127.0.0.1", auto_correct: true

    # Provider-specific configuration so you can fine-tune various
    # backing providers for Vagrant. These expose provider-specific options.
    # Example for VirtualBox:
    #
    config.vm.provider "virtualbox" do |vb|
      vb.name = "jenkins_vm"
      # Display the VirtualBox GUI when booting the machine
      # vb.gui = true

      # Customize the amount of memory on the VM:
      vb.memory = "8192"
      vb.cpus = 4
    end
    #
    # View the documentation for the provider you are using for more
    # information on available options.
  end

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: [".git/"]

  # Enable provisioning with a ansible_local script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # install and deploy docker
  config.vm.provision "docker"

  config.vm.provision "ansible_local" do |ansible|
    ansible.verbose = "v"
    ansible.playbook = "ansible/play.yml"
    ansible.groups = {
      "Git" => [NODE_A_HOST],
      "CI" => [NODE_B_HOST]
    }
    ansible.host_vars = {
      NODE_A_HOST => {"NODE_IP" => NODE_A_IP},
      NODE_B_HOST => {"NODE_IP" => NODE_B_IP}
    }
  end
end
