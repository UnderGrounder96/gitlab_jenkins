# docs

https://vagrantup.com/docs/multi-machine
https://vagrantup.com/docs/networking

# login credentials

```bash
# gitlab_user - root
# gitlab_password:
cat /etc/gitlab/initial_root_password


# jenkins_user - admin
# jenkins_password:
cat /var/lib/jenkins/secrets/initialAdminPassword
```

# Install gitlab (RHEL 8)

https://about.gitlab.com/install/#almalinux-8

# Install jenkins (RHEL 8)

https://jenkins.io/doc/book/installing/linux/

## sshd debug

https://phoenixnap.com/kb/ssh-permission-denied-publickey

## debug jinja2

{{ hostvars[item] | to_nice_json }}

{{ ansible_default_ipv4['address'] }} {{ ansible_hostname }} {{ ansible_hostname }}.local

{% for item in groups['all'] %}
{{ item[ansible_default_ipv4][address] }} {{ item[ansible_hostname] }} {{ item[ansible_hostname] }}.local
{% endfor %}

{% for host in groups['webservers'] %}
{{ hostvars[host]['ansible_default_ipv4']['address'] }} {{ hostvars[host]['ansible_fqdn'] }}
{% endfor %}

## How to allow multi-node Vagrant networking

There are multiple ways to achieve this:

```bash
# easiest method - install vagrant hosts plugin
vagrant plugin install vagrant-hosts

# then add this line to Vagrantfile
config.vm.provision "hosts", sync_hosts: true


## After defining the IPs and hostname using Vagrantfile variables. i.e.:
### configuration parameters ###
NODE_A_IP = "192.168.33.10"
NODE_A_HOST = "gitlab"

NODE_B_IP = "192.168.33.11"
NODE_B_HOST = "jenkins"

# dynamically set /etc/hosts mapping via shell provisioning
config.vm.provision "shell", env: {"NODE_A_IP"=>NODE_A_IP,"NODE_A_HOST"=>NODE_A_HOST,
"NODE_B_IP"=>NODE_B_IP,"NODE_B_HOST"=>NODE_B_HOST}, inline: <<-SHELL
  echo "$NODE_A_IP $NODE_A_HOST $NODE_A_HOST.local" >> /etc/hosts
  echo "$NODE_B_IP $NODE_B_HOST $NODE_B_HOST.local" >> /etc/hosts
SHELL

# alternatively one could configure multi-node networking using ansible provisioning and jinja2 template, check code
```
