#!/usr/bin/env bash

set -euo pipefail

datetime=$(date '+%F_%T')

# logger function
function _logger_info(){
    local datetime=$(date '+%c')
    echo -e "\n[$datetime]: $*...\n"
}


function vagrant_destroy(){
    _logger_info "WARNING: Destroying Virtual Machine"

    sleep 3s

    vagrant destroy -f
}

# trap vagrant_destroy EXIT

function main(){
    vagrant_destroy

    [ -f Vagrantfile ] || {
      _logger_info "There is no Vagrantfile"; exit 1
    }

    _logger_info "Starting the vagrant machine"
    vagrant up

    # _logger_info "Acquiring vagrant up logs"
    # vagrant scp ":/vagrant/logs/*.log" logs/
}


main 2>&1 | tee logs/vagrant_up_$datetime.log
